# tangent

### Simple matrix web chat

![Client](https://sanlox.dev/tangent/img/tanclient.png "Client")

### [Working example here](https://sanlox.dev/tangent/)

## Configs

### tan_room
matrix room to be joined on startup

e.g. `!xxxxxxxxxxxxxxxxxx:matrix.org`

this has to be a room id, not a room alias


### tan_hs
address of the matrix homeserver

e.g. `https://matrix.homeserver.tld`


### tan_ns
namespace of the client-server api

e.g. `/_matrix/client/r0/`

### tan_out
element to output data to

### tan_in
element to take input from

### tan_user
element to take username from

### tan_pass
element to take password from

### tan_guests
allow or disallow guest login

### tan_limit
number of messages to load on startup

## Functions

### tan_login()
login with data from tan_user and tan_pass

receive and set tan_token to use for further actions

start tan_join() on successful login

### tan_guest();
create a guest account and login with it

start tan_join() on successful login

### tan_logout()
invalidate tan_token

### tan_join()
join room set in tan_room

start tan_sync() on successful join

### tan_sync()
on startup reiceive last messages of tan_room and set batch

with batch set listen to new messages in tan_room

send all received messages to tan_print() and rerun

### tan_send()
send value of tan_in to tan_room and clear field

### tan_print()
print message to tan_out and scroll down

## Extras

### Send messages via enter key

    tan_in.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
        tan_send();
      }
    });

### Logout on page close

    window.onbeforeunload = function() {
      tan_logout();
    };
